/*******************
* FILE : linear_search.C
* Author : Shaswata
* Date : 30/10/2015
********************
* Problem : Implement Fibonacci searching technique
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
// #include <conio.h>

/*** GLOBAL DECLARATIONS ***/
static int length; // Store Length of array under operation
const static int F[47]={0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765,
             10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578,
             5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296,
             433494437, 701408733, 1134903170, 1836311903}; // An array of Fibonacci Numbers

/*** FUNCTION PROTOTYPES ***/
int fibanacciSearch(int*, int, int, int); // Perform the binary search operation of a given
void printArray(int*); // Function to print the array under operation

/* MAIN FUNCTION */
int main() {
	int *array, i = 0, key, result;
	// clrscr();
	printf("Enter Required Lenght for the array : ");
	scanf("%d", &length);

	// Allocate memory for array
	array = (int *) malloc(sizeof(int) * length);
	if(array == NULL) {
		printf("\nError : Memory Not Allocated!\n");
		// exit(1);
		return EXIT_FAILURE;
	}
	for(; i < length; i++ ) {
		printf("\nEnter element %d of %d : ", i+1, length);
		scanf("%d", &array[i]);
	}
	printArray(array);
	printf("\nEnter Search Key : \n");
	scanf("%d", &key);
	result = fibanacciSearch(array, key);
	if(result>=0)
		printf("KEY : %d IS FOUND AT INDEX %d.\n", key, result);
	else
		printf("KEY : %d IS NOT FOUND.\n", key);
	// getch();
	return EXIT_SUCCESS;
}

/** FUNCTION FIBANACCI_SEARCH
* 	@params
*		array : pointer to main array
*		key : search key
*		min : location of lower index
*		max : location of upper index
*/
int fibonacciSearch(int *array, int key, int min, int max) {
  // TODO
}

/* FUNCTION PRINT_ARRAY */
void printArray(int *array) {
	int i = 0;
	printf("\n\n[ ");
	for(; i < length ; i++ )
		printf("%d, ", array[i]);
	printf("\b\b ]\n\n");
}
