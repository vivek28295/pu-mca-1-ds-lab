/*******************
* FILE : INSERTIO.C
* Author : Shaswata
* Date : 29/11/2015
********************
* Problem : Implement Bubble Sort
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "../DISPLAY/print_array.h"
#include "bubble_sort.h"
// #include <conio.h>

/* MAIN FUNCTION*/
int main() {
	int *array, i = 0, length;
	// clrscr();
	printf("Enter Required Lenght for the array : ");
	scanf("%d", &length);

	// Allocate memory for array
	array = (int *) malloc(sizeof(int) * length);
	if(array == NULL) {
		printf("\nError : Memory Not Allocated!\n");
		// exit(1);
		return EXIT_FAILURE;
	}
	for(; i < length; i++ ) {
		printf("\nEnter element %d of %d : ", i+1, length);
		scanf("%d", &array[i]);
	}
	printf("\nOriginal Array : \n");
	printArray(array, length);
	bubbleSort(array, length, 'Y');
	printf("\n\nSorted Array : \n");
	printArray(array, length);
	// getch();
	return EXIT_SUCCESS;
}
