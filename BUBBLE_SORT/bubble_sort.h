/*******************
* FILE : INSERTIO.C
* Author : Shaswata
* Date : 29/11/2015
********************
* Problem : Implement Bubble Sort
********************/

#include "../DISPLAY/print_array.h"

#ifndef BUBBLE_SORT
#define BUBBLE_SORT

/** FUNCTION BUBBLE_SORT
* 	@params
*		array : pointer to main array
*		length : length of array to be sorted
*		debug : if 'Y' or 'y', then print debug info
*/

void bubbleSort(int *array, int length, char debug) {
	int i = 0, j, flag = 1, temp;
	while( i < length && flag == 1) {
		j = 0;
		flag = 0;
		while( j < length - i - 1 ) {
			if( array[j] > array[j+1] ) {
				flag = 1;
				temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
			}
			++j;
		}

		// DEBUG
		if(debug == 'Y' || debug == 'y') {
			printf("Pass %d : ", i+1);
			printArray(array, length);
		}

		++i;
	}
}

#endif
