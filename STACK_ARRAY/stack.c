/*******************
* FILE : stack.c
* Author : Shaswata
* Date : 07/11/2015
********************
* Problem : Implement stack using array
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
// #include <conio.h>


/* MAIN FUNCTION*/
int main() {
  STACK *stack = NULL;
  int size, top, element;
  char choice;
  printf("\nEnter size of Stack : ");
  scanf("%d", &size);
  if(size < 1) { // Invalid size
    fprintf(stderr, "Error : Invalid Size. Program terminated.\n");
    return EXIT_FAILURE;
  }
  // Create new stack
  stack = createStack(size);
  if(stack->stack == NULL) {
    fprintf(stderr, "Error : Memory could not be allocated for new stack. Try again!\n");
    return EXIT_FAILURE;
  }
  do {
    printf("\nChoose Operation :\n**********************************************");
    printf("\n1. PUSH.\n2. POP.\n3. TOP.\n4. Exit Program.\n\n");
    scanf(" %c", &choice);
    printf("\n\n");
    switch(choice) {
      case '1':
        // PUSH data on stack
        printf("\n\nEnter value to PUSH : ");
        scanf("%d", &element);
        if(PUSH(stack, element))
          printf("\nValue successfully PUSHED on stack.");
        else
          fprintf(stderr, "\nError : STACK OVERFLOW OCCURED!!\n\n");
        break;
      case '2':
        // POP data on stack
        if(top = POP(stack))
          printf("\nValue POPPED from stack : %d", top);
        else
          fprintf(stderr, "\nError : STACK UNDERFLOW OCCURED!!\n\n");
        break;
      case '3':
        // Show TOP element of stack
        top = getTOP(stack);
        if(top == -1)
          printf("\nSTACK IS EMPTY.\n");
        else
          printf("\nTOP : %d", top);
        break;
      case '4':
        // Exit program
        printf("\n\n***** THANK YOU *****\n\n");
        return EXIT_SUCCESS;
        break;
      default:
        printf("\n\aUNKNOWN INPUT! TRY AGAIN\n");
        break;
    }
  }while(1);
  return EXIT_SUCCESS;
}


// EOF
