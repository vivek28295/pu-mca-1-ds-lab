/*******************
* FILE : INSERTIO.C
* Author : Shaswata
* Date : 11/09/2015
********************
* Problem : Implement Insertion Sort
********************/

#include "../DISPLAY/print_array.h"


#ifndef INSERTION_SORT
#define INSERTION_SORT

/** FUNCTION INSERTION_SORT
* 	@params
*		array : pointer to main array
*		length : length of array to be sorted
*		debug : if 'Y' or 'y', then print debug info
*/

void insertionSort(int *array, int length, char debug) {
	int i = 1, j, temp;
	while( i < length ) {
		j = i;
		while( j > 0 && array[j-1] > array[j]) {
			/* PERFORM SWAP OPERATION */
			temp = array[j];
			array[j] = array[j-1];
			array[j-1] = temp;

			--j;
		}

		// DEBUG
		if(debug == 'Y' || debug == 'y') {
			printf("Pass %d : ", i);
			printArray(array, length);
		}

		i++;
	}
}

#endif
