/*******************
* FILE : INSERTIO.C
* Author : Shaswata
* Date : 02/11/2015
********************
* Problem : Print an array
********************/

#ifndef PRNT_ARR
#define PRNT_ARR

/* FUNCTION PRINT_ARRAY */
void printArray(int *array, int length) {
	int i = 0;
	printf("\n\n[ ");
	for(; i < length ; i++ )
		printf("%d, ", array[i]);
	printf("\b\b ]\n\n");
}

#endif
