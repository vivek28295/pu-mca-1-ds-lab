/*******************
* FILE : linear_search.C
* Author : Shaswata
* Date : 30/10/2015
********************
* Problem : Implement linear searching technique
********************/

#ifndef LINEAR_SEARCH
#define LINEAR_SEARCH


/* FUNCTION LINEAR_SEARCH */
int linearSearch(int *array, int key, int length) {
  int i = 0;
	printf("Searching for %d in array : \n", key);
	for(; i < length; i++) {
		if(array[i] == key)
			return i+1;
	}
	return 0;
}

#endif
