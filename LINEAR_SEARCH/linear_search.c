/*******************
* FILE : linear_search.C
* Author : Shaswata
* Date : 30/10/2015
********************
* Problem : Implement linear searching technique
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "../DISPLAY/print_array.h"
#include "linear_search.h"
// #include <conio.h>


/* MAIN FUNCTION*/
int main() {
	int *array, i = 0, key, result, length;
	// clrscr();
	printf("Enter Required Lenght for the array : ");
	scanf("%d", &length);

	// Allocate memory for array
	array = (int *) malloc(sizeof(int) * length);
	if(array == NULL) {
		printf("\nError : Memory Not Allocated!\n");
		// exit(1);
		return EXIT_FAILURE;
	}
	for(; i < length; i++ ) {
		printf("\nEnter element %d of %d : ", i+1, length);
		scanf("%d", &array[i]);
	}
	printArray(array, length);
	printf("\nEnter Search Key : \n");
	scanf("%d", &key);
	result = linearSearch(array, key, length);
	if(result)
		printf("KEY : %d IS FOUND AT INDEX %d.\n", key, result-1);
	else
		printf("KEY : %d IS NOT FOUND.\n", key);
	// getch();
	return EXIT_SUCCESS;
}
